#include "clkUtilities.h"
#include "queueUtilities.h"
#include<iostream>
#include<vector>
#include<string>
#include <string.h>
#include <algorithm>
#include <sstream>
#include <cstring>

/* Modify this file as needed*/
int remainingtime =0;
int id=0;
void waitforclk()
{
  int prev_clock=getClk();
  while(prev_clock==getClk()){}
  return;
}
void sendTerminationSignal(){

}
void handler(int num)
{
  printf("process intrubted\n");
}
int main(int agrc, char* argv[]) {
    //printf("%d -->",agrc);
    signal(SIGINT, handler);
    printf("process %s       : execTime: %s\n",argv[0], argv[1]);
    remainingtime = std::stoi(argv[1]);
    id=std::stoi(argv[0]);
    initClk();
    while(remainingtime>0) {
        //waitforclk();
       usleep(500 * 1000);
       remainingtime--;
       usleep(500 * 1000);
       //sleep();
       printf("process %s       : remaining Time -> %d\n",argv[0],remainingtime);
    }
    printf("process %s       : process terminated\n", argv[0]);
    //if you need to use the emulated clock uncomment the following line
    //destroyClk(false);
    exit(id);
    return 0;
}
