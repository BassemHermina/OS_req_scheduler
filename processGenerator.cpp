#include "clkUtilities.h"
#include "queueUtilities.h"
#include<vector>
#include<string>
#include <string.h>
#include <fstream>
#include <iostream>
using namespace std;

enum UserInput {
  BISHO_NULL,
  HPF,
  SRTN,
  RR
};
void ClearResources(int);
void readInputFile(string inputFileName);
char* getUserChoise();
void ClearMessageQueue();
int sendProcess(int currentTime); //return number of left processes to be sent
char *const parameters[]={(char*) 0};
char *const UserChoice[]={getUserChoise(),(char*) 0};  //Ask the user about the chosen scheduling Algorithm and its parameters if exists.
vector<processData> processes;
const char* me = "processGenerator: ";
int sch_pid;
bool exitt=false;
void waitforclk()
{

  int prev_clock=getClk();
  while(prev_clock==getClk()){}
  return;
}
void SIGCHILD_handler(int i)
{
  int stat,flag;
  flag = waitpid(sch_pid, &stat, WNOHANG);
  // if(WIFSTOPPED(stat)){printf("stoped %d\n",running_pid);}
  // if(WIFEXITED(stat)){}
  if(flag){
    printf("exited %d\n",WEXITSTATUS(stat));
    exitt=true;
  }else{
    printf("scheduler stoped %d\n",sch_pid);
    // sleep(2);
    // kill(sch_pid,SIGCONT);
  }

}
int main() {
    //printf("pidgrp: %d\n", getpgrp());
    signal(SIGCHLD, SIGCHILD_handler);  //TODO BISHOY TO NOT COMMENT THIS
    initQueue(true);
    //Initiate and create Scheduler and Clock processes.
    if (fork() == 0){
      //child
      execv("./clock.out",parameters);
      exit(0);
    }
    sch_pid=fork();
    if (sch_pid==0){
      //child
      if (execv("./sch.out",UserChoice) == -1) //it returns -1 if error
        printf("ERROR: branching error in scheduler.\n");
      exit(0);
    }
    initClk(); //use this function AFTER creating clock process to initialize clock, and initialize MsgQueue

    //TODO:
    //5ali balak lw mfish file y2ra meno by5araf w msh bykamel
    readInputFile("processes.txt"); //Creating a data structure for process  and  provide it with its parameters
    //5-Send & Notify the information to  the scheduler at the appropriate time
    //(only when a process arrives) so that it will be put it in its turn.

    int clockNow=0;
    int clockBefore=-1;
    while (true) {
      int clockNow= getClk();
      if (clockNow != clockBefore){
        printf("%s", me);
        printf("current time is %d\n",clockNow);
        clockBefore = clockNow;
        if (!sendProcess(clockNow)){ //send the current time processes only once per clock cycle
           //hna da m3nah en l sending queue 5elese 5alas
           //ana ba2fel l program dlwa2ty bs mafrod ma2feloosh
           sleep(1); // was pause (#2)
           //  msgctl(qid, IPC_RMID, (struct msqid_ds*)0);
           break;
        }
      }
      //sleep(1); // (#3)
    }

    //no more processes, send end of transmission message
    lastSend();
    pause();

    //////////To clear all resources
    // since en hna fi destroyClk y3ni el file da lazm yfdal shaghal , yb2a hyfdal fl while di
    ClearResources(0);
    //====================================

}

void ClearResources(int) {
    msgctl(qid, IPC_RMID, (struct msqid_ds*)0);
    destroyClk(true);
    exit(0);
}

void readInputFile(const string inputFileName) {
    ifstream input;
    const char * inputFileName_c = inputFileName.c_str();
    input.open(inputFileName_c);
    string s;
    input>>s;
    int temp;
    //cout<<s<<endl;  //bassem->bisho (1)

    while(getline(input,s))
    {
        //#id​ ​ ​ ​ ​ arrival​ ​ ​ ​ ​ runtime​ ​ ​ ​ ​ priority
        //intializing a process to be pushed in the processes vector
        //cout<<s<<endl; (#1)
        processData data;
        input>>temp;
        data.id=temp;
        input>>temp;
        data.arrivTime=temp;
        input>>temp;
        data.execTime=temp;
        input>>temp;
        data.priority=temp;
        data.state=NOTINIT;
        data.remaingTime=data.execTime;
        data.waitTime=0;
        data.forked=false;
        //push to the vector
        processes.push_back(data);
    }
    input.close();
    processes.pop_back(); //to solve an error in reading the file
  for(int i=0;i<processes.size();i++)//for testing only
    {
        cout<<processes[i].id<<"  "<<processes[i].arrivTime<<"    "<<processes[i].execTime<<"      "<<processes[i].priority<<endl;
    }

}

char* getUserChoise(){
  int no = 0;
  string userCho;
  //while the user have not typed 1 or 2 or 3 , ask him again for input
  while (no!=1 && no !=2 && no!=3){
    printf("%s", me);
    printf("Choose a scheduling algorithm: 1-HPF 2-SRTN 3-RR\n");
    scanf("%d",&no);
    if (no==1) userCho = "HPF\0";
    if (no==2) userCho = "SRTN\0";
    if (no==3) userCho = "RR\0";
    if (no!=1 && no !=2 && no!=3) printf("Wrong choice, ");
  }
  //convert the string to char*
  char * userCho_char = new char[userCho.size() + 1];
  strcpy ( userCho_char, userCho.c_str());
  return userCho_char;
}

int sendProcess(int currentTime){
  //search the "vector<processData> processes" for the processes
  //with arrival time == currentTime and send them
  for (int i = 0; i < processes.size(); i++){
    if (processes[i].arrivTime == currentTime )
      { //this process time has come
        printf("%s", me);
        printf("now sending process #%d", processes[i].id);
        processes[i].state = READY;
        int cc = Sendmsg(processes[i]);
        printf(" returned flag: %d\n", cc);
      }
    }

  int left=0;
  for (int i = 0; i < processes.size(); i++){
    if (processes[i].state != READY )
      left++;
  }
  printf("%s", me);
  printf("%d processes left\n", left);
  return left;
}

void ClearMessageQueue(){
  /*
      //TODO
      CALLED ON START
      CLEAR QUEUE as it may have had some messages not cleared from
      the last run time
  */
}
