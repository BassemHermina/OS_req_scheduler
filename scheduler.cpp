#include "clkUtilities.h"
#include "queueUtilities.h"
#include<iostream>
#include<vector>
#include<string>
#include <string.h>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <signal.h>
#include <fstream>
#include <cmath>

using namespace std;


bool Priority_sort(const processData &left,const processData &right){
  return left.priority > right.priority;
}
bool Remaining_sort(const processData &left,const processData &right){
  return left.remaingTime < right.remaingTime;
}
int startNewProcess(processData startingProcess);
void notifyProcessToStop(processData process);
void addProcessToTable(processData process);
void deleteProcessFromTable(processData process);
void deleteProcessFromTable(processData process);
void updateTableIndexOfProcess(processData process);
void fetchMessages(); //done
void updateReceivingVectorData();
void SRTN_updateProcessTable();
void stopProcess(processData &process);
void contProcess(processData &process);
void SIGCHILD_handler(int num);
void printProcessQueue();
void runFirstProcess_RR();void transitFirstProcesstoRecievingQueue();void processTerminationHandler_RR(int x);void runRR();int running_pid_RR;std::vector <processData> stats_RR;

void SIG_cont(int i)
{
  printf("scheduler continue handler\n");
}



void runProcess();
void logStatistics();
void updateTableSRTN();
void waitforclk()
{
  int prev_clock=getClk();
  while(prev_clock==getClk()){}
  return;
}
//TODO: function handler for child termination signal

std::vector <processData> receivingQueue;
std::vector <processData> processes_RR;
std::vector <processData> HPF_processes;
const char* me = "scheduler       : ";   std::string RRlog = ""; std::string STRNlog = ""; int endTime = 0;
std::vector<processData> STRN_processes;
int running_pid=0;
bool quantaFinished = false; bool ChildFinished = false; bool working = true;

std::vector<processData> deleted;
bool last_send=false;
int last_clk;

int main(int argc, char* argv[]) {
    initQueue(false);
    initClk();
    printf("%s", me);
    printf("User Selected: %s\n",argv[0]);
    if (argv[0][0] == 'H'){ // HPF algo
      system("> scheduler.log"); int startingTime,endTime; bool first = true;
      string HPFlog = "";
      while (HPF_processes.size()||receivingQueue.size()||!last_send){
        //start by reading the messages every once in loop
        fetchMessages();
        while (receivingQueue.size()){
          HPF_processes.push_back(receivingQueue[0]);
          receivingQueue.erase(receivingQueue.begin()+0);
        }
        std::sort(HPF_processes.begin(), HPF_processes.end(), Priority_sort);
        if (HPF_processes.size() != 0){
          //TODO updating the running process data
          if (first) {first = false; startingTime = getClk();}
          char * cc ;
          std::string ss = std::to_string(HPF_processes[0].id);
          cc = (char * const)ss.c_str();
          cc[ss.size()]='\0';

          char * const parameters [] = {(char*)cc,(char*)std::to_string(HPF_processes[0].remaingTime).c_str(),(char*)0};

          HPF_processes[0].state = RUNNING;
          HPF_processes[0].remaingTime = HPF_processes[0].execTime;
          // execTime da el moda eli mafrod yshtghalha
          HPF_processes[0].waitTime = getClk() - HPF_processes[0].arrivTime;
          HPF_processes[0].forked = true;
          HPF_processes[0].lastOnTime = getClk();
          //log file
          HPFlog = HPFlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(HPF_processes[0].id) + " started arr " + std::to_string(HPF_processes[0].arrivTime) + " total " + std::to_string(HPF_processes[0].execTime) + " remain " + std::to_string(HPF_processes[0].remaingTime) + " wait " + std::to_string(HPF_processes[0].waitTime) + "\n";
          //system(RRlog + " > scheduler.log");

          int ChildPID = fork();
          if (ChildPID == 0){
            //child
            execv("./process.out",parameters);
            exit(0);
          }
          int stat = 0;printf("%s", me);printf("starting to wait\n");
          if (ChildPID != 0)
            wait(&stat);
          printf("%s", me);printf("stopped waiting\n");
          endTime = getClk();
          int runTime222 = getClk() - HPF_processes[0].lastOnTime;
          HPF_processes[0].remaingTime -= runTime222;
          ////
          if (HPF_processes[0].remaingTime != 0)
            printf("%s ERROR! process remaining time != 0 on termination -> %d\n", me, HPF_processes[0].remaingTime);
          float TA = getClk() - HPF_processes[0].arrivTime;
          float WTA = TA / HPF_processes[0].execTime; //if execTime != 0
          //a5od awel 4 chars mn l string da
          HPFlog = HPFlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(HPF_processes[0].id) + " finished arr " + std::to_string(HPF_processes[0].arrivTime) + " total " + std::to_string(HPF_processes[0].execTime) + " remain " + std::to_string(HPF_processes[0].remaingTime) + " wait " + std::to_string(HPF_processes[0].waitTime)
                          + " TA " +  std::to_string(TA) + " WTA " + std::to_string(WTA) + "\n";

          //TODO 3aiz ashof leh awel quantum btb2a 4 clock ticks bs , we leeh fi wa7da fl nos bte5las we el snia el ba3dha l process el b3dha btebda2
          // string to char *
          char * systemCall ; //RRlog
          std::string ss3 = "";
          ss3 = ss3 + "echo " + "'" + HPFlog + "'" + "> 'scheduler.log'";
          systemCall = (char * const)ss3.c_str();
          systemCall[ss3.size()]='\0';

          system(systemCall);
          ////

          HPF_processes[0].pid = TA; //just to use in the next section
          HPF_processes[0].lastOnTime = WTA; //just to use in the next section
          //TODO hana5odha fi 7eta tania abl ma ne erase
          deleted.push_back(HPF_processes[0]);
          HPF_processes.erase(HPF_processes.begin()+0);
        }
      }

      float totalProcessesTime=0, avg_WTA=0, avg_wait=0, std_WTA=0;
      for (int i = 0; i < deleted.size(); i++){
        totalProcessesTime+= deleted[i].execTime;
        avg_WTA += deleted[i].lastOnTime; //lastOnTime = WTA
        avg_wait += deleted[i].pid;
      }
      avg_wait /= deleted.size();
      avg_WTA /= deleted.size();
      float CPUutil = (totalProcessesTime / getClk())*100;
      float stdd = 0;
      for (int i = 0; i < deleted.size(); i++){
        deleted[i].lastOnTime = avg_WTA - deleted[i].lastOnTime;
        deleted[i].lastOnTime = deleted[i].lastOnTime * deleted[i].lastOnTime;
        stdd = stdd + deleted[i].lastOnTime;
      }
      stdd = stdd / deleted.size();
      stdd = sqrt(stdd);
      std::string stats = "CPU​​ utilization=" + std::to_string(CPUutil) + "%\n" + "Avg​ ​WTA=" + std::to_string(avg_WTA) +  "\n" + "Avg​ Waiting=" + std::to_string(avg_wait) + "\n" + "Std​ ​ WTA= "+ std::to_string(stdd) +  "\n";

      // string to char *
      char * systemCall ; //RRlog
      std::string ss3 = "";
      ss3 = ss3 + "echo " + "'" + stats + "'" + "> 'scheduler.perf​'";
      systemCall = (char * const)ss3.c_str();
      systemCall[ss3.size()]='\0';

      system(systemCall);

    } else if (argv[0][0] == 'S'){ // SRTN algo
      while (STRN_processes.size()||receivingQueue.size()||!last_send){//SRTN
        //start by reading the messages every once in loop
        last_clk=getClk();
        signal(SIGCHLD, SIGCHILD_handler);
        signal(SIGCONT,SIG_cont);
        if (STRN_processes.size() != 0){
          STRN_processes[0].remaingTime--;
          STRNlog = STRNlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(STRN_processes[0].id) + " stopped arr " + std::to_string(STRN_processes[0].arrivTime) + " total " + std::to_string(STRN_processes[0].execTime) + " remain " + std::to_string(STRN_processes[0].remaingTime) + " wait " + std::to_string(STRN_processes[0].waitTime) + "\n";
          stopProcess(STRN_processes[0]);

        }

        fetchMessages();
        updateReceivingVectorData();
        SRTN_updateProcessTable();
        while (receivingQueue.size()){
          STRN_processes.push_back(receivingQueue[0]);
          receivingQueue.erase(receivingQueue.begin()+0);
        }
        std::sort(STRN_processes.begin(), STRN_processes.end(), Remaining_sort);
        for (int i=0;i<STRN_processes.size();i++)
        {
          printf("#id %d remainingtime %d\n",STRN_processes[i].id,STRN_processes[i].remaingTime);
        }
        if (STRN_processes.size() != 0){
          if (STRN_processes[0].forked==true){
            STRNlog = STRNlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(STRN_processes[0].id) + " resumed arr " + std::to_string(STRN_processes[0].arrivTime) + " total " + std::to_string(STRN_processes[0].execTime) + " remain " + std::to_string(STRN_processes[0].remaingTime) + " wait " + std::to_string(STRN_processes[0].waitTime) + "\n";
            contProcess(STRN_processes[0]);
            running_pid= STRN_processes[0].pid;
            //waitforclk();
          waitforclk();
          }else{

            STRN_processes[0].forked = true;
            STRN_processes[0].state = RUNNING;


            ///////////////////

            STRNlog = STRNlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(STRN_processes[0].id) + " started arr " + std::to_string(STRN_processes[0].arrivTime) + " total " + std::to_string(STRN_processes[0].execTime) + " remain " + std::to_string(STRN_processes[0].remaingTime) + " wait " + std::to_string(STRN_processes[0].waitTime) + "\n";


            //------------------------------
            char * cc ;
            std::string ss = std::to_string(STRN_processes[0].id);
            cc = (char * const)ss.c_str();
            cc[ss.size()]='\0';
            //------------------------------


            char * const parameters [] = {(char*)cc,(char*)std::to_string(STRN_processes[0].remaingTime).c_str(),(char*)0};
            int ChildPID = fork();
            if (ChildPID == 0){
              //child
              execv("./process.out",parameters);


            }else{
            running_pid= ChildPID;
            STRN_processes[0].pid=ChildPID;
            //waitforclk();
            waitforclk();

          }

          }

        }

      }

      float totalProcessesTime=0, avg_WTA=0, avg_wait=0, std_WTA=0;
      for (int i = 0; i < deleted.size(); i++){
        totalProcessesTime+= deleted[i].execTime;
        avg_WTA += deleted[i].lastOnTime;
        avg_wait += deleted[i].pid;
      }
      avg_wait /= deleted.size();
      avg_WTA /= deleted.size();
      float CPUutil = (totalProcessesTime / getClk())*100;
      float stdd = 0;
      for (int i = 0; i < deleted.size(); i++){
        deleted[i].lastOnTime = avg_WTA - deleted[i].lastOnTime;
        deleted[i].lastOnTime = deleted[i].lastOnTime * deleted[i].lastOnTime;
        stdd = stdd + deleted[i].lastOnTime;
      }
      stdd = stdd / deleted.size();
      stdd = sqrt(stdd);
      std::string stats = "CPU​​ utilization=" + std::to_string(CPUutil) + "%\n" + "Avg​ ​WTA=" + std::to_string(avg_WTA) +  "\n" + "Avg​ Waiting=" + std::to_string(avg_wait) + "\n" + "Std​ ​ WTA= "+ std::to_string(stdd) +  "\n";

      // string to char *
      char * systemCall ; //RRlog
      std::string ss3 = "";
      ss3 = ss3 + "echo " + "'" + stats + "'" + "> 'scheduler.perf​'";
      systemCall = (char * const)ss3.c_str();
      systemCall[ss3.size()]='\0';

      // system(systemCall); --> etba3 hna kman  : stats
      ofstream pref;
      pref.open("scheduler.perf");
      pref<<stats;
      pref.close();

    }
    else if (argv[0][0] == 'R'){ // RR algo
      runRR();
    }


    printf("now terminating scheduler\n");
    //===================================
    return 0;
}

void fetchMessages(){ //blocking function
  /**
    1- check for new message (NEW PROCESS STARTED)
    2- add to main vector
  **/
  int returnedFlag = 100; int lastFlag = -1;
  while (returnedFlag !=-1){ //-1 is returned on failure (nothing to recieve)
    struct processData pD;
    returnedFlag = Recmsg(pD);
    //if (returnedFlag!=lastFlag) {lastFlag = returnedFlag; printf("%s", me); printf("returnedFlag changed, returnedFlag = %d", returnedFlag);}
    if (returnedFlag != -1 && returnedFlag!=1){
      printf("%s", me);
      printf("received process of id #%d , priority: %d\n",pD.id, pD.priority);
      pD.state = READY;
      pD.waitTime = getClk() - pD.arrivTime;
      receivingQueue.push_back(pD);

    } else if(returnedFlag == 1){last_send=true;}
  }
}

void updateReceivingVectorData(){
  for(int i =0;i<receivingQueue.size();i++)
    receivingQueue[i].waitTime=getClk()-receivingQueue[i].arrivTime;
}
void SRTN_updateProcessTable()
{
  for(int i =0;i<STRN_processes.size();i++){
    STRN_processes[i].waitTime=getClk()-STRN_processes[i].arrivTime-STRN_processes[i].execTime+STRN_processes[i].remaingTime;
  //  if(STRN_processes[i].state==RUNNING){STRN_processes[i].remaingTime--;}
    //if(STRN_processes[i].remaingTime==0){STRN_processes.erase(STRN_processes.begin()+i);}
    /**
      EL WOGOOODY
    */
  }
}
void stopProcess(processData &process)
{


  if (process.state==RUNNING)
    {
      process.state=READY;
      kill(process.pid,SIGSTOP);
    }
}
void contProcess(processData &process)
{
  if (process.state==READY)
    {
      process.state=RUNNING;
      kill(process.pid,SIGCONT);
    }

}
void SIGCHILD_handler(int num)
{
      int stat,flag;
      flag = waitpid(running_pid, &stat, WNOHANG);
      // if(WIFSTOPPED(stat)){printf("stoped %d\n",running_pid);}
			// if(WIFEXITED(stat)){}
      if(flag){
        float TA = getClk() - STRN_processes[0].arrivTime;
        float WTA = TA / STRN_processes[0].execTime; //if execTime != 0
        //a5od awel 4 chars mn l string da
        STRNlog = STRNlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(STRN_processes[0].id) + " finished arr " + std::to_string(STRN_processes[0].arrivTime) + " total " + std::to_string(STRN_processes[0].execTime) + " remain " + std::to_string(STRN_processes[0].remaingTime) + " wait " + std::to_string(STRN_processes[0].waitTime)
                        + " TA " +  std::to_string(TA) + " WTA " + std::to_string(WTA) + "\n";

        //TODO 3aiz ashof leh awel quantum btb2a 4 clock ticks bs , we leeh fi wa7da fl nos bte5las we el snia el ba3dha l process el b3dha btebda2
        // string to char *
        char * systemCall ; //RRlog
        std::string ss3 = "";
        ss3 = ss3 + "echo " + "'" + STRNlog + "'" + "> 'scheduler.log'";
        systemCall = (char * const)ss3.c_str();
        systemCall[ss3.size()]='\0';

        //printf("%s\n", systemCall);
        //system(systemCall); --> etba3 da msh hena : STRNlog
        ofstream log;
        log.open("scheduler.log");
        log<<STRNlog;
        log.close();
        ////

        STRN_processes[0].pid = TA; //just to use in the next section
        STRN_processes[0].lastOnTime = WTA; //just to use in the next section
        deleted.push_back(STRN_processes[0]);
        STRN_processes.erase(STRN_processes.begin()+0);
        printf("exited %d\n",WEXITSTATUS(stat));
      }else{
        printf("stoped %d\n",running_pid);


      }
}
void SIGCHILD_handler_RR(int num)
{
      //printf("gali signal ya3ammmmm\n");
      /// ehhhhhhhhel ghaltaaaaaaa HENAAAAAAAAAAAAAAA!!!!!!!! 5araaaaaaaaaa //m3lsh
      int stat,flag;
      flag = waitpid(running_pid_RR, &stat, WNOHANG);
  		if(flag){
        printf("terminated %d\n",running_pid_RR);
        ChildFinished = true;
        working = true;
      }
}

void runFirstProcess_RR(){
  if (processes_RR[0].forked == true){
    printf("%s", me);
    printf("continuing process\n");

    //gwa continue process function nafsaha
    //processes_RR[0].state = RUNNING;

    //processes_RR[0].remaingTime = processes_RR[0].execTime; // msh mn hna , wana ba pause a7san
    //el wait = el wa2t dlwa2ty - ana geet emta (da kol 7yati asln) - el 7ba el eshtghalthom fihom (y3ni execTime - remaingTime)
    processes_RR[0].waitTime = getClk() - processes_RR[0].arrivTime - (processes_RR[0].execTime - processes_RR[0].remaingTime); //3aiz at2aked mn da
    processes_RR[0].lastOnTime = getClk();
    RRlog = RRlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(processes_RR[0].id) + " resumed arr " + std::to_string(processes_RR[0].arrivTime) + " total " + std::to_string(processes_RR[0].execTime) + " remain " + std::to_string(processes_RR[0].remaingTime) + " wait " + std::to_string(processes_RR[0].waitTime) + "\n";
    //system(RRlog + " > scheduler.log");

    contProcess(processes_RR[0]);
    running_pid_RR = processes_RR[0].pid;
  } else {
    //first parameter
    char * cc ;
    std::string ss = std::to_string(processes_RR[0].id);
    cc = (char * const)ss.c_str();
    cc[ss.size()]='\0';
    //second parameter
    char * cc2 ;
    std::string ss2 = std::to_string(processes_RR[0].remaingTime);
    cc2 = (char * const)ss2.c_str();
    cc[ss.size()]='\0';
    /////////////
    char * const parameters [] = {(char*)cc,(char*)cc2,(char*)0};
    processes_RR[0].state = RUNNING;
    processes_RR[0].remaingTime = processes_RR[0].execTime;
    // execTime da el moda eli mafrod yshtghalha
    processes_RR[0].waitTime = getClk() - processes_RR[0].arrivTime;
    processes_RR[0].forked = true;
    processes_RR[0].lastOnTime = getClk();
    //log file
    RRlog = RRlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(processes_RR[0].id) + " started arr " + std::to_string(processes_RR[0].arrivTime) + " total " + std::to_string(processes_RR[0].execTime) + " remain " + std::to_string(processes_RR[0].remaingTime) + " wait " + std::to_string(processes_RR[0].waitTime) + "\n";
    //system(RRlog + " > scheduler.log");

    printf("%s", me);
    printf("forking process\n");
    int ChildPID = fork();
    processes_RR[0].pid = ChildPID;
    running_pid_RR = ChildPID;
    if (ChildPID == 0){
      //child
      execv("./process.out",parameters);
    }
    return;
  }
}
void transitFirstProcesstoRecievingQueue(){
  receivingQueue.push_back(processes_RR[0]);
  processes_RR.erase(processes_RR.begin()+0);
}
void processTerminationHandler_RR(int x){

  //kill(getpid(),SIGCONT);
}
void printProcessQueue(){
  printf("%s", me);
  for (int i = 0; i < processes_RR.size(); i++){
    printf("#%d - ", processes_RR[i].id);
    printf("forked: %d - ", (int)processes_RR[i].forked);
    printf("state: %d - ", (int)processes_RR[i].state);
    printf("pid: %d", (int)processes_RR[i].pid);
  }
  printf("\n");
}
void runRR(){
  system("> scheduler.log"); int startingTime,endTime; bool first = true;
  signal(SIGCHLD, SIGCHILD_handler_RR); //di ghalat , sigChild di bte7sal lma y pause brdo  msh y exit bas
  int currentTime=0, lastTime=0;
  while(true){
    currentTime = getClk();
    if (currentTime != lastTime){
      printf("%s", me);
      printf("currentTime: %d\n",currentTime);
      lastTime = currentTime;
    }
    fetchMessages();
    while (receivingQueue.size()){
      processes_RR.push_back(receivingQueue[0]);
      receivingQueue.erase(receivingQueue.begin()+0);
      //printProcessQueue();
    }
    if (processes_RR.size()){
      if (first) {first = false; startingTime = getClk();}
      runFirstProcess_RR();
      //printf("returned from forking !!!!!!!!!!!!!\n");
      //TODO ozbot alarm
      printf("%s", me);
      printf("stopping myself\n");

      int currentclockcounterforalarm = getClk();
      int clockcounteralarm = getClk() + 6;

      //alarm(6);
      working = false;
      //pause();
      while (currentclockcounterforalarm!= clockcounteralarm && !working){
        currentclockcounterforalarm = getClk();
        if (currentclockcounterforalarm == clockcounteralarm){
          printf("%s", me);
          printf("my alarm has just signaled me\n");
          quantaFinished = true;
          working = true;
        }
          //alarm(0);
      }
      //while(!working); //wait for the signal handler to finish it's code
      printf("%s", me);
      printf("statring myself\n");
    }
    // ----------- ON RETURN FROM SLEEP
    //case quanta finished
    if (quantaFinished){
      //working = true;
      printf("%s",me );
      printf("stopping process #%d - pid: %d\n",processes_RR[0].id, processes_RR[0].pid);
      //stopProcess(processes_RR[0]);
      kill(processes_RR[0].pid,SIGSTOP);
      processes_RR[0].state = READY;
      processes_RR[0].remaingTime -= (getClk() - processes_RR[0].lastOnTime);
      RRlog = RRlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(processes_RR[0].id) + " stopped arr " + std::to_string(processes_RR[0].arrivTime) + " total " + std::to_string(processes_RR[0].execTime) + " remain " + std::to_string(processes_RR[0].remaingTime) + " wait " + std::to_string(processes_RR[0].waitTime) + "\n";
      //system(RRlog + " > scheduler.log");

      //el wa2t el fadeli =
      fetchMessages();
      transitFirstProcesstoRecievingQueue(); //TODO  / 5ali balak lazm azbot el data
      quantaFinished = false;

    } //case child terminated
    else if (ChildFinished){
      //working = true;
      processes_RR[0].remaingTime -= (getClk() - processes_RR[0].lastOnTime);
      if (processes_RR[0].remaingTime != 0)
        printf("%s ERROR! process remaining time != 0 on termination -> %d\n", me, processes_RR[0].remaingTime);
      float TA = getClk() - processes_RR[0].arrivTime;
      float WTA = TA / processes_RR[0].execTime; //if execTime != 0
      //a5od awel 4 chars mn l string da
      RRlog = RRlog + "At time " + std::to_string(getClk()) + " process " + std::to_string(processes_RR[0].id) + " finished arr " + std::to_string(processes_RR[0].arrivTime) + " total " + std::to_string(processes_RR[0].execTime) + " remain " + std::to_string(processes_RR[0].remaingTime) + " wait " + std::to_string(processes_RR[0].waitTime)
                    + " TA " +  std::to_string(TA) + " WTA " + std::to_string(WTA) + "\n";

      //TODO 3aiz ashof leh awel quantum btb2a 4 clock ticks bs , we leeh fi wa7da fl nos bte5las we el snia el ba3dha l process el b3dha btebda2
      // string to char *
      char * systemCall ; //RRlog
      std::string ss3 = "";
      ss3 = ss3 + "echo " + "'" + RRlog + "'" + "> 'scheduler.log'";
      systemCall = (char * const)ss3.c_str();
      systemCall[ss3.size()]='\0';

      system(systemCall);

      //TODO save data of process
      printf("%s",me );
      printf("erasing process #%d \n",processes_RR[0].id);
      stats_RR.push_back(processes_RR[0]);
      stats_RR[stats_RR.size()-1].pid = TA; //just to use in the next section
      stats_RR[stats_RR.size()-1].lastOnTime = WTA; //just to use in the next section
      processes_RR.erase(processes_RR.begin()+0);
      ChildFinished = false;
      endTime = getClk();
    }

    if (!receivingQueue.size() && !processes_RR.size() && last_send){
      printf("clockkkkkkkkkk: %d\n", getClk());
      printf("breakinggggggggggggggggggg\n");
      break;
    }
  }


  float totalProcessesTime=0, avg_WTA=0, avg_wait=0, std_WTA=0;
  for (int i = 0; i < stats_RR.size(); i++){
    totalProcessesTime+= stats_RR[i].execTime;
    avg_WTA += stats_RR[i].lastOnTime;
    avg_wait += stats_RR[i].pid;
  }
  avg_wait /= stats_RR.size();
  avg_WTA /= stats_RR.size();
  float CPUutil = (totalProcessesTime / getClk())*100;
  float stdd = 0;

  for (int i = 0; i < stats_RR.size(); i++){
    stats_RR[i].lastOnTime = avg_WTA - stats_RR[i].lastOnTime;
    stats_RR[i].lastOnTime = stats_RR[i].lastOnTime * stats_RR[i].lastOnTime;
    stdd = stdd + stats_RR[i].lastOnTime;
  }
  stdd = stdd / stats_RR.size();
  stdd = sqrt(stdd);
  std::string stats = "CPU​​ utilization=" + std::to_string(CPUutil) + "%\n" + "Avg​ ​WTA=" + std::to_string(avg_WTA) +  "\n" + "Avg​ Waiting=" + std::to_string(avg_wait) + "\n" + "Std​ ​ WTA= "+ std::to_string(stdd) +  "\n";

  // string to char *
  char * systemCall ; //RRlog
  std::string ss3 = "";
  ss3 = ss3 + "echo " + "'" + stats + "'" + "> 'scheduler.perf​'";
  systemCall = (char * const)ss3.c_str();
  systemCall[ss3.size()]='\0';

  system(systemCall);
}
